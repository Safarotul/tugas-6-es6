//soal 1
const luas = (p , l) => {
    return p * l
}
const keliling = (p, l) => {
    return 2*(p + l)
}

console.log(luas(5, 3));
console.log(keliling(5,3));


//soal 2
function newFunction(firstName, lastName) {
    return {
        firstName,
        lastName,
        fullName() {
            return (firstName + " " + lastName) 
        }
    }
}
newFunction("William", "Imoh").fullName()



//soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const {firstName,lastName,address,hobby} = newObject

console.log(firstName, lastName, address, hobby);



//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)




//soal 5
const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} `

console.log(before)



